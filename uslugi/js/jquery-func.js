$(document).ready(function(){ 

   	//=================================== Slide ===================================//
	
	$('#da-slider').cslider({
 	   autoplay    : true,
	    interval    : 4000  	
	});

	//=================================== IMAGE HOVER  =================================//	  
	$('.img-preview').each(function() {
	    $(this).hover(
	    function() {
	        $(this).stop().animate({ opacity: 0.2 }, 400);
	    },
	    function() {
	    	$(this).stop().animate({ opacity: 1.0 }, 400);
	     })
	});

	//=================================== carousel ===================================//
	  
	  $('.flexslider').flexslider({
        animation: "slide",
        animationLoop: false,
        itemWidth: 210,
        itemMargin: 5,
        minItems: 2,
        maxItems: 6,
        start: function(slider){
          $('body').removeClass('loading');
        }
      });



	//=================================== Totop  ===================================//
	$().UItoTop({ 		
		scrollSpeed:500,
		easingType:'linear'
	});
	



	//=================================== Ligbox  ===================================//

	    jQuery("a[class*=fancybox]").fancybox({
		'overlayOpacity'	:	0.7,
		'overlayColor'		:	'#000000',
		'transitionIn'		: 'elastic',
		'transitionOut'		: 'elastic',
		'easingIn'      	: 'easeOutBack',
		'easingOut'     	: 'easeInBack',
		'speedIn' 			: '700',
		'centerOnScroll'	: true
	});
	
	jQuery("a[class*='et_video_lightbox']").click(function(){
		var et_video_href = jQuery(this).attr('href'),
			et_video_link;

		et_vimeo = et_video_href.match(/vimeo.com\/(.*)/i);
		if ( et_vimeo != null )	et_video_link = 'http://player.vimeo.com/video/' + et_vimeo[1];
		else {
			et_youtube = et_video_href.match(/watch\?v=([^&]*)/i);
			if ( et_youtube != null ) et_video_link = 'http://youtube.com/embed/' + et_youtube[1];
		}
		
		jQuery.fancybox({
			'overlayOpacity'	:	0.7,
			'overlayColor'		:	'#000000',
			'autoScale'		: false,
			'transitionIn'	: 'elastic',
			'transitionOut'	: 'elastic',
			'easingIn'      : 'easeOutBack',
			'easingOut'     : 'easeInBack',
			'type'			: 'iframe',
			'centerOnScroll'	: true,
			'speedIn' 			: '700',
			'href'			: et_video_link
		});
		return false;
	});



});


	//=================================== Login Box  ===================================//

	$('.login').click(function(){

		if ($('.login_box').css('top') == "-300px")
		{
			$top = "70px";			
		} else {
			$top = "-300px";
		}

		$('.login_box').animate({
			top: $top
		},{
			duration: 500,
			easing: "easeInOutExpo"
		});


	});

	$(function(){
		$('.login_box').fadeIn();		

		if ($.cookie('displayoptions') == "1")
		{
			$('.login_box').css('top','-300px');
		} else if ($.cookie('displayoptions') == "0") {
			$('.login_box').css('top','0');
		} else {
			$('.login_box').delay(800).animate({
				top: "-300px"
			},{
				duration: 500,
				easing: "easeInOutExpo"
			});

			
		}		

	});